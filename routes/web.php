<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/listes', 'listesController@store');

Route::get('/listes/{listeid}', 'listesController@show');

//Fix that one, affiche tous les items d'une liste avec une liste en parametres
Route::get('/listes/{liste}', 'listesController@show');

Route::get('/listes/create', 'ListesController@create');

Route::get('/listes/create{listeName}', 'ListesController@store');

//Route::get('/listes/{liste}/items/{item}}', 'ItemsController@show');
