@extends('layouts.layout')

@section('nav')
    @parent
@endsection
@section('content')
    @if(!empty($data))
        {{$data}}
    @endif
@endsection